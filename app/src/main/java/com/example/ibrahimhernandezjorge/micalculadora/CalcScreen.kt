package com.example.ibrahimhernandezjorge.micalculadora

import android.os.Parcel
import android.os.Parcelable
import android.widget.TextView

class CalcScreen() : Parcelable {
    var pantalla: TextView? = null
    var valorActual = 0.0
    var valorHide = 0.0
    var memoria: Double? = null

    constructor(parcel: Parcel) : this() {
        valorActual = parcel.readDouble()
        valorHide = parcel.readDouble()
        memoria = parcel.readValue(Double::class.java.classLoader) as? Double
    }

    fun refresh() {
        pantalla!!.text = valorActual.toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(valorActual)
        parcel.writeDouble(valorHide)
        parcel.writeValue(memoria)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CalcScreen> {
        override fun createFromParcel(parcel: Parcel): CalcScreen {
            return CalcScreen(parcel)
        }

        override fun newArray(size: Int): Array<CalcScreen?> {
            return arrayOfNulls(size)
        }
    }
}