package com.example.ibrahimhernandezjorge.micalculadora

import android.support.v7.app.AppCompatActivity
import android.graphics.Typeface
import android.os.Bundle
import android.widget.Button
import kotlin.math.*
import android.content.Intent
import android.content.DialogInterface
import android.support.v7.app.AlertDialog


class MainActivity : AppCompatActivity() {
    private var pantalla: CalcScreen? = null
    private var operando: Boolean = false
    private var decimal: Boolean = false
    private var puntoFlotante: Double = 0.1
    private var currentOp: String = ""
    private var igualando: Boolean = false
    private var multipleOperador: Boolean = false

    companion object {
        const val SUMA = "+"
        const val RESTA = "-"
        const val MULTIPLICACION = "*"
        const val DIVISION = "/"
        const val RAIZ = "sqrt"
        const val POTENCIA = "^"
        const val PORCENTAJE = "%"
        const val COSENO = "cos"
        const val SENO = "sin"
        const val POTENCIA_2 = "^2"
        const val LOG = "log"
        const val LOG_2 = "log2"
        const val FACTORIAL = "!"
        const val ENTREX = "1/X"
        const val PI = Math.PI
        const val BUNDLE_KEY = "bundle_key_pantalla"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        pantalla = CalcScreen()

        iniciarElementos()
        pantalla!!.refresh()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putParcelable(BUNDLE_KEY, pantalla)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        pantalla = savedInstanceState!!.getParcelable(BUNDLE_KEY)
        iniciarElementos()
        pantalla!!.refresh()
    }

    private fun iniciarElementos() {
        pantalla!!.pantalla = findViewById(R.id.pantalla)
        pantalla!!.pantalla!!.typeface = Typeface.createFromAsset(assets, "fonts/digital-7.ttf")
        findViewById<Button>(R.id.mPlusButton).setOnClickListener { _ ->
            pantalla!!.valorHide = pantalla!!.valorActual
            pantalla!!.valorActual = pantalla!!.memoria!!

            multipleOperador = false
            operando = true
            currentOp = SUMA
            operar()
            operando = false
        }

        findViewById<Button>(R.id.mMinusButton)?.setOnClickListener { _ ->
            pantalla!!.valorHide = pantalla!!.valorActual
            pantalla!!.valorActual = pantalla!!.memoria!!

            multipleOperador = false
            operando = true
            currentOp = RESTA
            operar()
            operando = false
        }

        findViewById<Button>(R.id.mButton).setOnClickListener { _ ->
            pantalla!!.memoria = pantalla!!.valorActual
            operando = true
        }

        findViewById<Button>(R.id.mClearButton)?.setOnClickListener { _ ->
            pantalla!!.memoria = 0.0
        }

        findViewById<Button>(R.id.cButton).setOnClickListener { _ ->
            resetPuntoFlotante()
            pantalla!!.valorActual = 0.0
            pantalla!!.valorHide = 0.0
            pantalla!!.refresh()
            operando = false
            decimal = false
            multipleOperador = false
            igualando = false
        }

        findViewById<Button>(R.id.ceButton).setOnClickListener { _ ->
            resetPuntoFlotante()
            pantalla!!.valorActual = 0.0
            pantalla!!.refresh()
        }

        findViewById<Button>(R.id.sumaButton).setOnClickListener { _ ->
            addOperador(SUMA)
        }

        findViewById<Button>(R.id.restaButton).setOnClickListener { _ ->
            addOperador(RESTA)
        }

        findViewById<Button>(R.id.multButton).setOnClickListener { _ ->
            addOperador(MULTIPLICACION)
        }

        findViewById<Button>(R.id.divButton).setOnClickListener { _ ->
            addOperador(DIVISION)
        }

        findViewById<Button>(R.id.potButton).setOnClickListener { _ ->
            addOperador(POTENCIA_2)
        }

        findViewById<Button>(R.id.raizButton).setOnClickListener { _ ->
            addOperador(RAIZ)
        }

        findViewById<Button>(R.id.porcentajeButton).setOnClickListener { _ ->
            addOperador(PORCENTAJE)
        }

        findViewById<Button>(R.id.cosButton)?.setOnClickListener{ _ ->
            addOperador(COSENO)
        }

        findViewById<Button>(R.id.sinButton)?.setOnClickListener { _ ->
            addOperador(SENO)
        }

        findViewById<Button>(R.id.multPotButton)?.setOnClickListener { _ ->
            addOperador(POTENCIA)
        }

        findViewById<Button>(R.id.logButton)?.setOnClickListener { _ ->
            addOperador(LOG)
        }

        findViewById<Button>(R.id.log2Button)?.setOnClickListener { _ ->
            addOperador(LOG_2)
        }

        findViewById<Button>(R.id.factorialButton)?.setOnClickListener { _ ->
            addOperador(FACTORIAL)
        }

        findViewById<Button>(R.id.entreXButton)?.setOnClickListener { _ ->
            addOperador(ENTREX)
        }

        findViewById<Button>(R.id.piButton)?.setOnClickListener { _ ->
            pantalla!!.valorActual = PI
            pantalla!!.refresh()
        }

        findViewById<Button>(R.id.number9).setOnClickListener { _ ->
            addNumber(9.0)
        }

        findViewById<Button>(R.id.number8).setOnClickListener { _ ->
            addNumber(8.0)
        }

        findViewById<Button>(R.id.number7).setOnClickListener { _ ->
            addNumber(7.0)
        }

        findViewById<Button>(R.id.number6).setOnClickListener { _ ->
            addNumber(6.0)
        }

        findViewById<Button>(R.id.number5).setOnClickListener { _ ->
            addNumber(5.0)
        }

        findViewById<Button>(R.id.number4).setOnClickListener { _ ->
            addNumber(4.0)
        }

        findViewById<Button>(R.id.number3).setOnClickListener { _ ->
            addNumber(3.0)
        }

        findViewById<Button>(R.id.number2).setOnClickListener { _ ->
            addNumber(2.0)
        }

        findViewById<Button>(R.id.number1).setOnClickListener { _ ->
            addNumber(1.0)
        }

        findViewById<Button>(R.id.number0).setOnClickListener { _ ->
            addNumber(0.0)
        }

        findViewById<Button>(R.id.igualButton).setOnClickListener { _ ->
            multipleOperador = false
            operando = true
            if(operadorBinario(currentOp))
                operar()
            operando = false
        }

        findViewById<Button>(R.id.puntoButton).setOnClickListener{ _ ->
            decimal = true
        }

        findViewById<Button>(R.id.signoButton).setOnClickListener{ _ ->
            pantalla!!.valorActual *= -1
            pantalla!!.refresh()
        }

        findViewById<Button>(R.id.offButton).setOnClickListener { _ ->
            val dialogClickListener = DialogInterface.OnClickListener { _, which ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE ->
                        System.exit(0)
                }
            }

            val builder = AlertDialog.Builder(this)
            builder.setMessage(R.string.off_dialog).setPositiveButton(R.string.si, dialogClickListener)
                    .setNegativeButton(R.string.no, dialogClickListener).show()
        }
    }

    private fun operadorBinario(op: String): Boolean {
            return (op != PORCENTAJE && op != RAIZ && currentOp != COSENO && currentOp != SENO
                    && currentOp != POTENCIA_2 && currentOp != LOG && currentOp != FACTORIAL
                    && currentOp != LOG_2 && currentOp != ENTREX)
    }

    private fun addNumber(number: Double) {
        when {
            operando || igualando -> {
                pantalla!!.valorActual = number
                operando = false
                igualando = false
            }
            decimal -> {
                pantalla!!.valorActual = pantalla!!.valorActual + (number * puntoFlotante)
                puntoFlotante /= 10
            }
            else ->
                pantalla!!.valorActual = pantalla!!.valorActual * 10 + number
        }
        pantalla!!.refresh()
    }

    private fun addOperador(op: String) {
        resetPuntoFlotante()

        if(multipleOperador) {
            operando = true
            operar()
            pantalla!!.valorHide = pantalla!!.valorActual
            currentOp = op
        }
        else {
            if(ordenOpAltera()) {
                pantalla!!.valorActual += pantalla!!.valorHide
                pantalla!!.valorHide = pantalla!!.valorActual - pantalla!!.valorHide
                pantalla!!.valorActual -= pantalla!!.valorHide
            }
            currentOp = op
            if(operadorBinario(op))
                multipleOperador = true

            if (operadorBinario(op)) {
                igualando = false
                operando = true
                pantalla!!.valorHide = pantalla!!.valorActual
            } else
                operar()
        }
    }

    private fun ordenOpAltera(): Boolean {
        return (currentOp == RESTA || currentOp == DIVISION)
    }

    private fun operar() {
        val valor = pantalla!!.valorActual

        when (currentOp) {
            SUMA -> pantalla!!.valorActual += pantalla!!.valorHide
            RESTA -> pantalla!!.valorActual = pantalla!!.valorHide - pantalla!!.valorActual
            MULTIPLICACION -> pantalla!!.valorActual *= pantalla!!.valorHide
            DIVISION -> pantalla!!.valorActual = pantalla!!.valorHide / pantalla!!.valorActual
            POTENCIA_2 -> pantalla!!.valorActual = pantalla!!.valorHide.pow(2)
            POTENCIA -> pantalla!!.valorActual = pantalla!!.valorHide.pow(pantalla!!.valorActual)
            PORCENTAJE -> pantalla!!.valorActual /= 100
            RAIZ -> pantalla!!.valorActual = sqrt(pantalla!!.valorActual)
            COSENO -> pantalla!!.valorActual = round(cos(Math.toRadians(pantalla!!.valorActual)))
            SENO -> pantalla!!.valorActual = round(sin(Math.toRadians(pantalla!!.valorActual)))
            LOG -> pantalla!!.valorActual = log10(pantalla!!.valorActual)
            LOG_2 -> pantalla!!.valorActual = log2(pantalla!!.valorActual)
            FACTORIAL -> pantalla!!.valorActual = factorial(pantalla!!.valorActual)
            ENTREX -> pantalla!!.valorActual = 1.0 / pantalla!!.valorActual
        }
        pantalla!!.refresh()

        if(ordenOpAltera()){
            pantalla!!.valorHide = pantalla!!.valorActual
            pantalla!!.valorActual = valor
        }

        else if(!igualando) {
            pantalla!!.valorHide = valor
            igualando = true
        }
    }

    private fun factorial(number: Double): Double {
        return when (number) {
            0.0 -> 0.0
            1.0 -> 1.0
            else -> number * factorial(number - 1.0)
        }
    }

    private fun resetPuntoFlotante() {
        if(decimal) {
            decimal = false
            puntoFlotante = 0.1
        }
    }
}
